#!/bin/bash
set -ex
mkdir -p code
pushd code
    tar jxf ../Magento-CE-2*.tar.bz2
    find var vendor pub/static pub/media app/etc -type f -exec chmod g+w {} \;
    find var vendor pub/static pub/media app/etc -type d -exec chmod g+ws {} \;
    # chown -R :<web server group> .
    chmod u+x bin/magento
popd
docker-compose build
docker-compose up -d
xdg-open http://magento2.localtest.me/setup/  
docker-compose logs  -f 